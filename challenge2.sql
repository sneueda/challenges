DELIMITER //

CREATE PROCEDURE vwap(
  IN start_date VARCHAR(12)
)
BEGIN

select sum(vol*close)/sum(vol) AS VWAP,
date_format( str_to_date(start_date,"%Y%m%d%H%i"), "%d-%m-%Y" ) AS Date,
date_format( str_to_date(start_date,"%Y%m%d%H%i"), "%H:%i" ) AS "Start Time",
date_format(DATE_ADD(str_to_date(start_date,"%Y%m%d%H%i"),INTERVAL 5 HOUR),"%H:%i") AS "End Time"
FROM sd2
WHERE str_to_date(date,"%Y%m%d%H%i") between
str_to_date(start_date,"%Y%m%d%H%i") and 
DATE_ADD(str_to_date(start_date,"%Y%m%d%H%i"),INTERVAL 5 HOUR);

END //

DELIMITER ;